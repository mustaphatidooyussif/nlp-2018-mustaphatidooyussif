import csv 
import numpy as np 
import pandas as pd 
import math
import random
import operator
from string import punctuation
from nltk.probability import FreqDist
import argparse

class NaiveBayesDocumentClassifier:
    """Naive Bayes Document Classifier. 
    
    This is a custom naive bayes classifier for classifying documents to different 
    classes. For the most part, this classifier classifiers documents into positive 
    document or negative document."""
    
    def __init__(self, documents = None):
        self.dataset = documents
        self.seperated_docs = {}
        self.categories = {}
        self.loglikelihood ={}
        self.logprior = {}
        self.V=[]
        self.class_vos ={}

    def parse_arguments(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('filepath', help='File name of text to summarize')
        args = parser.parse_args()
        return args

    def prep_data(self, file_name, separator=','):
        
        #Read data file
        documents = pd.read_csv(file_name, sep=separator, names = ['document', 'label'], dtype=str).apply(lambda x: x.astype(str).str.lower())
        if self.dataset is None: self.dataset = documents
        return self.dataset

    def train_model(self, features, labels):
        prior = self._logprior(labels)
        self._vocabulary_list(features, labels)
        word_count = self.word_frequency(self.V)  
        word_count0 = self.word_frequency(self.class_vos['0'])  
        word_count1 = self.word_frequency(self.class_vos['1']) 


        total_voc =self.word_counts(word_count)
        total_neg_voc = self.word_counts(word_count0)
        total_pos_voc = self.word_counts(word_count1)
        
        self.likelihood_probability(word_count0, word_count1, word_count,total_voc, total_neg_voc,total_pos_voc)

    def _logprior(self, labels):
        """This calculates the prior probabilities."""
        for label in labels:
            self.categories[label] = self.categories.get(label, 0) + 1
            
        total = 0
        for i in self.categories:
            total += self.categories[i]

        
        for label in self.categories:
            self.logprior[label] = math.log(self.categories[label]/total)
        return self.logprior

    def likelihood_probability(self, word_count0, word_count1, word_count,total_voc, total_neg_voc,total_pos_voc):

        for label in self.categories:
            for word in word_count:
                if label =='0':
                    self.loglikelihood[word+','+label]=math.log((word_count0.get(word, 0)+1)/(total_neg_voc + total_voc))
                else:
                    self.loglikelihood[word+','+label]=math.log((word_count1.get(word, 0)+1)/(total_pos_voc + total_voc))

    def predicted_classes(self, testdoc,logprior,loglikelihood,C,V):
        test_result =[]
        for testdoc in testdoc:
            testdoc = self.replace_chars(testdoc)
            val = self._test_naive_bayes(testdoc,logprior,loglikelihood,C,V)
            test_result.append(val)
        return test_result

    def replace_chars(self, doc):
        
        chars= set(list(punctuation))
        for char in chars:
            doc = doc.replace(char, '')
        return doc

    def _vocabulary_list(self, features, labels):
        for doc, label in zip(features, labels):
            doc = self.replace_chars(doc)
            self.V += doc.split()
            if label not in self.class_vos:
                self.class_vos[label] = []
            self.class_vos[label] += doc.split()

    def word_frequency(self, documents): 
        word_count={}
        for word in documents:
            word_count[word]= word_count.get(word, 0)+1
        return word_count

    def word_counts(self, word_count):  
        count_w =0
        for word in word_count:
            count_w += word_count[word]   
        return count_w

    def _test_naive_bayes(self,testdoc, logprior,loglikelihood, C, V):
        sum={}
        for c in C:
            sum[c] = logprior[c]
            for word in testdoc.split():
                if word in V:
                    sum[c] = sum[c]+ loglikelihood[word +','+ c]
        return max(sum.items(), key=operator.itemgetter(1))[0]

    def predict(self, testdoc):
        """This predicts the classes of the  documents passed."""
        V = self.word_frequency(self.V)  
        if isinstance(testdoc, list):
            test_result = self.predicted_classes(testdoc,self.logprior,self.loglikelihood,self.categories,V)
        elif testdoc.lower().endswith(('.txt', '.csv')):
            testdoc = open(testdoc, 'r')
            test_result = self.predicted_classes(testdoc,self.logprior,self.loglikelihood,self.categories,V)
        else:
            test_result= self._test_naive_bayes(testdoc,self.logprior,self.loglikelihood,self.categories,V)
        return test_result

    def find_prior_probabilities(self):

        #Group documents into their classified classes.
        for label in self.dataset['label']:
            if label not in self.seperated_docs: 
                self.seperated_docs[label] = []
            self.seperated_docs[label].append(self.dataset['document'].values.tolist())
        
        #Find the total documents for each class.
        class_doc_count = {}
        for label in self.seperated_docs:
            if label not in class_doc_count:
                class_doc_count[label] = len(self.seperated_docs[label])

        #Calculate the Prior probabilities for each class 
        prior = {}
        for doc_count in class_doc_count:
            if doc_count not in prior: prior[doc_count] = math.log(class_doc_count[doc_count]/len(self.dataset))

        return self.seperated_docs

    def vocabulary(self):
        docs = []
        for doc in self.dataset['document']:
            docs.append(doc.split()) 

        voc={}
        for i in range(len(docs)):
            for j in docs[i]:
                if len(j) > 1:voc[j] =voc.get(j, 0)+1 

        return voc 

    def _vocabulary_per_class(self, documents):
        docs = []
        for doc in documents:
            print(doc)

    def _vocabulary(self, documents):
        docs = []
        for doc in documents:
            if not isinstance(doc, list): 
                docs.append(doc.split())
            else: 
                docs.append(doc[0].split())

        vocs ={}
        for i in range(len(docs)):
            for j in docs[i]:
                if len(j)>1:
                    vocs[j] = vocs.get(j, 0)+1 
        return vocs
    
    def measure_accuracy(self, predicted, actual):
        count = 0
        for i in range(len(actual)):
            if predicted[i] == actual[i]:
                count += 1 
        return count * 1.0/ len(actual)

    def merge_files(self, files_list):

        #Merging data frames into one bigfile. 
        df_list = []
        for filename in files_list:
            df = pd.read_csv(filename, sep='\t', names = ['document', 'label'], dtype=str).apply(lambda x: x.astype(str).str.lower())
            df_list.append(df)

        df = pd.concat(df_list, axis=0, join='inner')
        return df

    def split_to_train_test(self, documents, test_ratio = 0.20):
        
        #doc_copy = self.dataset.values.tolist() 

        train_set = []
        test = []
        labels = []
        train_labels = []
        for label, doc in zip(documents['label'], documents['document']): 
            test.append(doc)
            labels.append(label)
        
        num_of_train_docs = int(len(test) * (1-test_ratio)) #compute the number of documents to train on
        
        #split the documents into testing and training dataset.
        while len(train_set) < num_of_train_docs:  
            index = random.randrange(20)  
            train_set.append(test.pop(index))  #randomly add documents to training document set
            train_labels.append(labels.pop(index)) #randomly add the documents classes to training class set
            
        return train_set, train_labels, test, labels

    def write_results_to_file(self, predicted):
        #Writing the predicted classes into a text file.
        f = open('results.txt', 'w')
        for result in predicted:
            f.write(result+'\n')

def main():
    file_name = 'yelp_labelled.txt'
    test_doc = 'testdoc.txt'
    model = NaiveBayesDocumentClassifier()
    args = model.parse_arguments()
    big_frame =model.merge_files(['yelp_labelled.txt', 'imdb_labelled.txt', 'amazon_cells_labelled.txt'])
    dataset = model.prep_data(file_name, separator ='\t')
    train_docs, train_labels, test_docs, test_labels = model.split_to_train_test(big_frame, test_ratio = 0.25)
    model.train_model(train_docs, train_labels)
    predicted = model.predict(args.filepath)
    model.write_results_to_file(predicted)
    #print(predicted)
    #accuracy = model.measure_accuracy(predicted,test_labels)
if __name__=='__main__':
    main()