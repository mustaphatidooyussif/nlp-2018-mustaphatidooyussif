import csv 
import math 

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
################################################################################
count = {}
for label in dataset['label']:
    count[label] = count.get(label, 0)+1
    
total = 0
for i in count:
    total +=count[i]

logprior = {}
for label in count:
    logprior[label] = math.log(count[label]/total)
    
##################################################################################
def word_frequency(documents): #use this to factor word count code below
    word_count={}
    for i in range(len(documents)):
        for j in documents[i]:
            if len(j)>1:
                word_count[j]= word_count.get(j, 0)+1
    return word_count
            
def replace_chars(doc):
    chars=[',', '.', '"', "'"]
    for char in chars:
        doc = doc.replace(char, ' ')
    return doc
  
def vobulary_list():
    V=[]
    class_vos ={}
    for doc, label in zip(dataset['document'],dataset['label']):
        doc = replace_chars(doc)
        V.append(doc.split())
        if label not in class_vos:
            class_vos[label] = []
        class_vos[label].append(doc.split())
    return V, class_vos

V=[]
class_vos ={}
for doc, label in zip(dataset['document'],dataset['label']):
    doc = replace_chars(doc)
    V.append(doc.split())
    if label not in class_vos:
        class_vos[label] = []
    class_vos[label].append(doc.split())


word_count={}
for i in range(len(V)):
    for j in V[i]:
        if len(j)>1:
            word_count[j]= word_count.get(j, 0)+1


word_count0={}
for i in range(len(class_vos['0'])):
    for j in class_vos['0'][i]:
        if len(j)>1:
            word_count0[j]= word_count0.get(j, 0)+1
            
word_count1={}
for i in range(len(class_vos['1'])):
    for j in class_vos['1'][i]:
        if len(j)>1:
            word_count1[j]= word_count1.get(j, 0)+1
            
################################################################################
def word_counts(word_count):  #use this to factor the code below
    count_w =0
    for word in word_count:
        count_w += word_count[word]
    return count_w
    
count_w =0
for word in word_count0:
    count_w += word_count0[word]

count2 =0
for word in word_count1:
    count2 += word_count1[word]

 
count3 =0
for word in word_count:
    count3 += word_count[word]


def likelihood_probability(count, word_count, clss_word_count, count_w, count3): #use this to factor the code below
    loglikelihood ={}
    for label in count:
        for word in word_count:
            loglikelihood[word+','+label]=math.log((clss_word_count.get(word, 0)+1)/(count_w + count3))
    return loglikelihood

loglikelihood ={}
for label in count:
    for word in word_count:
        loglikelihood[word+','+label]=math.log((word_count0.get(word, 0)+1)/(count_w + count3))

####################################################################################################
#test_doc = ['graphics is far from the best part of the game']
test_doc = 'testdoc.txt'
def _test_naive_bayes(testdoc, logprior,loglikelihood, C, V):
    sum={}
    for c in count:
        sum[c] = logprior[c]
        for word in testdoc:
            if word in V:
                sum[c] = sum[c]+ loglikelihood[word, c]
                