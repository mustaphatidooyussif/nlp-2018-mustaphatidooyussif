"""
Yussif Mustapha Tidoo. 

MINIMUN EDIT DISTANCE IMPLEMENTATION
------------------------------------
This program takes two words as command line
arguments and computes the differences between 
them. That is, minimum of edits( deletion, insertion
,insertion) it takes to convert on word to other.
"""

import argparse 
import numpy as np 
import tabulate as tb

def main():
    args = parse_arguments()
    matrix, min_edits = min_edit_distance(args.source, args.target)
    print(min_edits)  #Minimum edits

    #NB: Uncomment this to draw a table of the edit distances.
    # print('The edit distance matrix tabulated below:')
    #tabulate_result(args.source, args.target,matrix) #Call the tabulate results function to present the 
                                                      # edit distance matrix nicely. 
                                                      

def parse_arguments(): #command line arguments parser
    parser = argparse.ArgumentParser()
    parser.add_argument('source', help='Source word')
    parser.add_argument('target', help='target word')
    args = parser.parse_args()

    return args

def min_edit_distance(source, target):
    n = len(source)  #  Length of the source word(rows).
    m = len(target)  #  Length of the target word(colums).

    #Initialize matrix D (m x n matrix). 
    D = np.zeros(shape=(n+1, m+1), dtype=np.int) 
    for i in range(1, n+1):
        D[i][0] = D[i-1][0] + 1
                                    #Distance from the empty string. 
    for j in range(1, m+1):
        D[0][j] = D[0][j-1] + 1 

    # Recurrence relation
    for i in range(1, n+1):
        for j in range(1, m+1):
            if source[i-1] == target[j-1]:
                inst_cost = 0    #No cost if characters are the same.
            else:
                inst_cost = 2
    
            D[i][j] = min(
                D[i-1][j] + 1, # deletion
                D[i][j-1] + 1, # insertion
                D[i-1][j-1] + inst_cost  # substitution
            )
    edits = '\nThe minimum edit distance between {} and {} is {}.\n'.format(source, target, D[n][m])
    return D, edits

def tabulate_result(source, target, data):
    source = '#' + source.lower()  #Row 1 header 
    target = '#' + target.lower()  #Column 1 header

    table = [[" "] + list(target)]
    for i in range(len(source)):
        row = [source[i]]
        for j in range(len(target)):
            row.append(data[i,j])
        table.append(row)

    print(tb.tabulate(table, tablefmt='grid'))


if __name__=='__main__':
    main()