import argparse 
import numpy as np 
import tabulate as tb

def main():
    args = parse_arguments()
    d_matrix, b_matrix = min_edit_distance(args.source, args.target)
    backtrace_inds = backtrace(b_matrix)

    print('* marks the edit distance.')
    tabulate_results(args.source, args.target,d_matrix, b_matrix, backtrace_inds)

def parse_arguments(): #command line arguments parser
    parser = argparse.ArgumentParser()
    parser.add_argument('source', default="", help='Source word')
    parser.add_argument('target',  default="", help='target word')
    args = parser.parse_args()

    return args

def min_edit_distance(source, target):
    n = len(source)  #  Length of the source word(rows).
    m = len(target)  #  Length of the target word(colums).

    #Initialize matrix D (m x n matrix). 
    D = np.zeros(shape=(n+1, m+1), dtype=np.int) 

    # B is a backtrace matrix which consisting tuple of binary digits
    # representing deletion, insertion and substitution respectively.

    B = np.zeros(shape=(n+1, m+1), dtype=[("del", 'b'), 
                      ("sub", 'b'),
                      ("ins", 'b')])
    
    B[1:,0] = (1, 0, 0)   #Initialize the first row to have (del=1, sub=0 and ins=0).
    B[0,1:] = (0, 0, 1)   #Initialize the firts column to have (del=0, sub=0 and ins=1).

    for i in range(1, n+1):
        D[i][0] = D[i-1][0] + 1
                                    #Distance from the empty string. 
    for j in range(1, m+1):
        D[0][j] = D[0][j-1] + 1 

    # Recurrence relation
    for i in range(1, n+1):
        for j in range(1, m+1):
            if source[i-1] == target[j-1]:  
                inst_cost = 0         #No cost if characters are the same
            else:
                inst_cost = 2

            deletion = D[i-1][j] + 1  # deletion
            insertion = D[i][j-1] + 1 # substitution
            substitution = D[i-1][j-1] + inst_cost  # insertion 
            med = min(
                deletion, 
                substitution,  
                insertion   
            )

            #If edit is deletion, insert tuple (1, 0, 0) to B at i, j.
            #If edit is subsition, insert tuple (0, 1, 0) to B at i, j.
            #If edit is insertion, insert tuple(0, 0, 1) to B at i, j.
            B[i,j] = (deletion==med, substitution==med, insertion==med)
            D[i][j] = med

    return D, B

def backtrace(b_matrix):
    n = b_matrix.shape[0]-1 #number of rows in the backtrace matric minus 1
    m = b_matrix.shape[1]-1 #number of columns in the backtrace matric minus 1
    backtrace_indexes = [(n, m)]  #start from the last cell ie.D[i,j]

    while (n, m) != (0 , 0):   #Base case
        if b_matrix[n,m][0]:   # if deletion, move left
            n, m = n-1, m
        elif b_matrix[n,m][1]:  #if substitution, move left and up
            n, m = n-1, m-1 
        elif b_matrix[n,m][2]:  #if insertion, move top
            n, m = n, m-1 
        backtrace_indexes.append((n, m))
    return backtrace_indexes

def tabulate_results(source, target, d_matrix, b_matrix, backtrace):
    source = '#' + source.lower()  #Row 1 header 
    target = '#' + target.lower()  #Column 1 header

    table = [[" "] + list(target)]
    for i in range(len(source)):
        row = [source[i]]
        for j in range(len(target)):
            deletion, substitution, insertion = b_matrix[i,j]

            #unicode arrows decoded (⇧, ⇖ and ⇦ ). This may not display well on a command line
            direction = ( chr(8679)+' ' if deletion else '') + \
                        ( chr(8662) + ' ' if substitution else '') + \
                        ( chr(8678) +' ' if insertion else '')

            med = str(d_matrix[i,j])
            cell_content = ' {} {} {}'.format(direction, med, '*' if (i,j) in backtrace else '')
            row.append(cell_content)
        table.append(row)
    print(tb.tabulate(table, tablefmt='grid'))

if __name__=='__main__':
    main()