import argparse
import numpy as np
import pandas as pd
from string import punctuation
from sklearn.feature_extraction import stop_words
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer 
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import  MultinomialNB
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

#This function reads the content of the test file to a list
def read_test_file(filename):  
    data = []
    f = open(filename, 'r')
    for line in f:              
        data.append(line)  #append each document to the data list 
    return data 

#Merging data frames into one bigfile. 
def merge_files():
    files_list = ['imdb_labelled.txt', 'yelp_labelled.txt', 'amazon_cells_labelled.txt']
    df_list = []
    for filename in files_list:
        df = pd.read_csv(filename, sep='\t', names = ['document', 'label'], dtype=str)
        df_list.append(df)
    df = pd.concat(df_list, axis=0, join='inner')   #Merge the dataframes together.
    df = df.applymap(str)
    return df


# Removing all stop words and punctuations from a given sentence
# and changing the sentence to lowercase sentence
def replace_chars(doc):
    stopwords = set(list(stop_words.ENGLISH_STOP_WORDS) + ['\t', '\n'])
    punctuations = list(punctuation)
    for char in punctuations:        #Replace all punctuation marks with an empty string
        doc = doc.replace(char, '')
    words_filtered = [word for word in doc.lower().split() if word not in stopwords and len(word) > 2]
    return ' '.join(words_filtered)

# Normalizing all the documents by remiving punctuations, stopwords, 
#changeing them to lowercase. It returns list of documents. 
def normalize(documents, labels):
    norm_data = []
    norm_labels = []
    for document, label in zip(documents,labels):
        document = replace_chars(document)
        norm_data.append(document)
        norm_labels.append(label)
    return norm_data, np.array(norm_labels)

#Returning list of documents
def unnormalize(documents, labels):
    un_norm_data = []
    un_norm_labels = []
    for document, label in zip(documents,labels):
        un_norm_data.append(document)
        un_norm_labels.append(label)
    return un_norm_data, np.array(un_norm_labels)


def naive_bayes(train_docs, train_labels, test_docs):
    #Extract features by
    #changing documents to sparse matrix form.
    count_vect = CountVectorizer()   
    X_new_counts = count_vect.fit_transform(train_docs)  
    tfidf_transformer = TfidfTransformer()            
    X_train_tfidf = tfidf_transformer.fit_transform(X_new_counts)

    #Train the model 
    clf = MultinomialNB().fit(X_train_tfidf, train_labels)

    # Transform the test document(vectorize the test document)
    test_vect = count_vect.transform(test_docs)
    test_vect_idf = tfidf_transformer.transform(test_vect)

    #Predicting
    predicted = clf.predict(test_vect_idf)
    return predicted

def logistic_regression(train_docs, train_labels, test_docs):
    #Extract features by
    #changing documents to sparse matrix form.
    count_vect = CountVectorizer()   
    X_new_counts = count_vect.fit_transform(train_docs)  
    tfidf_transformer = TfidfTransformer()            
    X_train_tfidf = tfidf_transformer.fit_transform(X_new_counts)

    #Train the model 
    clf = LogisticRegression().fit(X_train_tfidf, train_labels)

    # Transform the test document(vectorize the test document)
    test_vect = count_vect.transform(test_docs)
    test_vect_idf = tfidf_transformer.transform(test_vect)

    #Predicting
    predicted = clf.predict(test_vect_idf)
    return predicted

# Write the results into a text file
def write_to_file(file_name, data):
    with open(file_name, 'w') as f:
        for line in data:
            f.write(line + '\n')

#This function generates the score of the model
#using precison, recal and f1
#Parameters: list of expected classes and list of predicted classes.
def model_score(expected, predicted):
    print(classification_report(expected, predicted))

#Main function
def main():
    args = parse_arguments()  
    classifier_type = args.type  #get the classifier type from the command line
    version = args.version   #get the classifier version from commmand line
    file_name = args.file #Get test file from commandline
    dataset = merge_files()

    # Testing
    # X_train, X_test, y_train, y_test = train_test_split(dataset.document, dataset.label, test_size=0.20, random_state=42)
    normalized_data, normalized_label = normalize(dataset.document, dataset.label)  #normalized data
    unnormalized_data, unnormalized_label = unnormalize(dataset.document, dataset.label) #unnormalized data
    test_docs = read_test_file(file_name)  #read the testfile content

    # test_docs = X_test
    # expected = y_test

    #Naive bayes classifier
    if classifier_type == "nb":
        if version == "n": #classifier normalize data using naive base if, type is nb and version is n
            predictions = naive_bayes(normalized_data, normalized_label, test_docs)
            write_to_file("results-nb-n.txt", predictions)

        elif version == "u":
            predictions = naive_bayes(unnormalized_data, unnormalized_label, test_docs)
            write_to_file("results-nb-u.txt", predictions)

        else:
            raise Exception("Improper version type")

    #Logistic regression classifier
    elif classifier_type == "lr":
        if version == "n":
            predictions = logistic_regression(normalized_data, normalized_label, test_docs)
            write_to_file("results-lr-n.txt", predictions)

        elif version == "u":
            predictions = logistic_regression(unnormalized_data, unnormalized_label, test_docs)
            write_to_file("results-lr-u.txt", predictions) 
            
        else:
            raise Exception("Improper version type")
    else:
        raise Exception("Program does not support classifier type")


def parse_arguments(): #command line arguments parser
    parser = argparse.ArgumentParser()
    parser.add_argument('type', help='classifier type- nb or lr')
    parser.add_argument('version', help='classifier version- n or u')
    parser.add_argument('file', help='the test file name e.g testfile.txt')
    args = parser.parse_args()

    return args

if __name__=="__main__":
    main()
